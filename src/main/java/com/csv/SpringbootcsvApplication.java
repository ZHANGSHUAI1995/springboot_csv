package com.csv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@EnableConfigurationProperties
@EntityScan("com.csv.pojo")
@ComponentScan("com.csv.service")
@ComponentScan("com.csv.mapper")
public class SpringbootcsvApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootcsvApplication.class, args);
    }

}
