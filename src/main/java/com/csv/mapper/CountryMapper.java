package com.csv.mapper;

import com.csv.pojo.Country;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CountryMapper extends JpaRepository<Country, Integer> {
}
