package com.csv.service;

import com.csv.mapper.CountryMapper;
import com.csv.pojo.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @ClassName CountryService
 * @Description TODO
 * @Author zhangshuai
 * @Date 2019/4/22 23:40
 * @Version 1.0
 **/
@Service
public class CountryService {

    @Autowired
    private CountryMapper countryMapper;

    public List<Country> findAll() {
        return countryMapper.findAll();
    }
}
