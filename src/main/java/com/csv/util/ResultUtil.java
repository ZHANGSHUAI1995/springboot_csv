package com.csv.util;


import com.csv.pojo.Result;

/**
 * @ClassName ResultUtil
 * @Description TODO
 * @Author zhangshuai
 * @Date 2019/4/22 23:10
 * @Version 1.0
 **/
public class ResultUtil {

    public static Result failure(String msg) {
        Result result = new Result(msg);
        return result;
    }
}
