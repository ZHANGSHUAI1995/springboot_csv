package com.csv.pojo;

/**
 * @ClassName Result
 * @Description TODO
 * @Author zhangshuai
 * @Date 2019/4/22 23:25
 * @Version 1.0
 **/
public class Result {
    private Object msg;

    public Object getMsg() {
        return msg;
    }

    public void setMsg(Object msg) {
        this.msg = msg;
    }

    public Result(Object msg) {
        this.msg = msg;
    }

    public Result() {
    }

}
