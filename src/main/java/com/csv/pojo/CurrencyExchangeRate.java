package com.csv.pojo;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @ClassName CurrencyExchangeRate
 * @Description TODO
 * @Author zhangshuai
 * @Date 2019/4/22 23:13
 * @Version 1.0
 **/
@Entity
@Table(name = "currency_exchange_rate", schema = "bison", catalog = "")
public class CurrencyExchangeRate {
    private int currencyExchangeRateId;
    private int currencyId;
    private double exchange;
    private Timestamp uTime;

    @Id
    @Column(name = "currency_exchange_rate_id")
    public int getCurrencyExchangeRateId() {
        return currencyExchangeRateId;
    }

    public void setCurrencyExchangeRateId(int currencyExchangeRateId) {
        this.currencyExchangeRateId = currencyExchangeRateId;
    }

    @Basic
    @Column(name = "currency_id")
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "exchange")
    public double getExchange() {
        return exchange;
    }

    public void setExchange(double exchange) {
        this.exchange = exchange;
    }

    @Basic
    @Column(name = "u_time")
    public Timestamp getuTime() {
        return uTime;
    }

    public void setuTime(Timestamp uTime) {
        this.uTime = uTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CurrencyExchangeRate that = (CurrencyExchangeRate) o;
        return currencyExchangeRateId == that.currencyExchangeRateId &&
                currencyId == that.currencyId &&
                Double.compare(that.exchange, exchange) == 0 &&
                Objects.equals(uTime, that.uTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyExchangeRateId, currencyId, exchange, uTime);
    }
}
