package com.csv.pojo;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Objects;

/**
 * @ClassName Currency
 * @Description TODO
 * @Author zhangshuai
 * @Date 2019/4/22 23:13
 * @Version 1.0
 **/
@Entity
public class Currency {
    private int currencyId;
    private String currencyName;
    private String currencyCode;
    private int exchangeUpdate;

    @Id
    @Column(name = "currency_id")
    public int getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(int currencyId) {
        this.currencyId = currencyId;
    }

    @Basic
    @Column(name = "currency_name")
    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    @Basic
    @Column(name = "currency_code")
    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    @Basic
    @Column(name = "exchange_update")
    public int getExchangeUpdate() {
        return exchangeUpdate;
    }

    public void setExchangeUpdate(int exchangeUpdate) {
        this.exchangeUpdate = exchangeUpdate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currency currency = (Currency) o;
        return currencyId == currency.currencyId &&
                exchangeUpdate == currency.exchangeUpdate &&
                Objects.equals(currencyName, currency.currencyName) &&
                Objects.equals(currencyCode, currency.currencyCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(currencyId, currencyName, currencyCode, exchangeUpdate);
    }
}
